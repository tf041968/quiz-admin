﻿CREATE TABLE Categories (
	Id int IDENTITY NOT NULL,
	Name nvarchar(64) NOT NULL UNIQUE,
	CONSTRAINT pk_categories PRIMARY KEY (Id)
)
GO

CREATE TABLE Questions (
	Id int IDENTITY,
	Description ntext,
	Blob varbinary(max),
	Filename nvarchar(260),
	CONSTRAINT pk_questions PRIMARY KEY (Id)
)
GO

CREATE TABLE Answers (
	Id int IDENTITY,
	Description ntext,
	Blob varbinary(max),
	Filename nvarchar(260),
	Correct bit NOT NULL,
	QuestionId int NOT NULL,
	CONSTRAINT pk_answers PRIMARY KEY (Id),
	CONSTRAINT fk_answers_question FOREIGN KEY (QuestionId) REFERENCES Questions (Id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
)
GO

CREATE TABLE QuestionInCategory (
	QuestionId int NOT NULL,
	CategoryId int,
	CONSTRAINT fk_qc_question FOREIGN KEY (QuestionId) REFERENCES Questions (Id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT fk_qc_category FOREIGN KEY (CategoryId) REFERENCES Categories (Id)
		ON DELETE SET NULL
		ON UPDATE CASCADE,
)
GO

CREATE TABLE Highscore (
	Score bigint,
	Name nvarchar(128),
	CategoryId int,
	CONSTRAINT fk_highscore_category FOREIGN KEY (CategoryId) REFERENCES Categories (Id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
)
GO

CREATE TABLE ApiKeys (
	[Id] int IDENTITY,
	[Shared] varchar(64) NOT NULL,
	--Secret varbinary(64) NOT NULL,
	--Salt varbinary(64) NOT NULL,
	[Type] tinyint NOT NULL,
	CONSTRAINT pk_apikey PRIMARY KEY (Id),
)
GO

CREATE UNIQUE NONCLUSTERED INDEX idx_apikeys
	ON ApiKeys (Shared)
GO

CREATE TABLE SessionApiKeys (
	 ApiKey int,
	 SessionKey varchar(64),
	 CONSTRAINT fk_session_apikey FOREIGN KEY (ApiKey) REFERENCES ApiKeys (Id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
)
GO

/*
CREATE TABLE AdminApiKeys (
	Id int IDENTITY,
	ApiKey int,
	CONSTRAINT pk_adminkey PRIMARY KEY (Id),
	CONSTRAINT fk_admin_apikey FOREIGN KEY (ApiKey) REFERENCES ApiKeys (Id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
)
GO
*/