﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Tracing;

using NLog;

namespace QuizAdmin {
	public static class LogConfig {
		public static void Register(HttpConfiguration config) {
			config.Services.Replace(typeof(ITraceWriter), new NLogger());
		}
	}

	public class NLogger : ITraceWriter {
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		private static readonly Dictionary<TraceLevel, Action<string>> LogActionMap =
			new Dictionary<TraceLevel, Action<string>> {
				{ TraceLevel.Debug, Logger.Debug },
				{ TraceLevel.Info,  Logger.Info  },
				{ TraceLevel.Warn,  Logger.Warn  },
				{ TraceLevel.Error, Logger.Error },
				{ TraceLevel.Fatal, Logger.Fatal }
			};

		public void Trace(HttpRequestMessage request, string category, TraceLevel level, Action<TraceRecord> traceAction) {
			var record = new TraceRecord(request, category, level);
			traceAction(record);
			Log(record);
		}

		private void Log(TraceRecord record) {
			LogActionMap[record.Level](record.Message);
		}
	}
}