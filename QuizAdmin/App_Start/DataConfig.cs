﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using QuizAdmin.Data.Common;

namespace QuizAdmin {
	public static class DataConfig {
		public static void Register() {
			ConnectionProviderManager.AddProvider(new SqlConnectionProvider(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString));
		}
	}
}