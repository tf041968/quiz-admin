﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using QuizAdmin.Data;

namespace QuizAdmin.Controllers
{
    [Authorize]
    public class CategoryController : ApiController
    {
        CategoryManager categorymanager;
        
        public IEnumerable<Category> GetCategories()
        {
            categorymanager = new CategoryManager();
            List<Category> results = categorymanager.GetCategories();
            return results;
        }
        //public Category GetCategory(int id)
        //{
        //    catmngr = new CategoryManager();
        //    Category result = catmngr.GetCategoryByID(id);
        //    return result;
        //}

        public HttpResponseMessage GetQuestionsFromCategory(int id, string last = "false", int limit = 10, bool category = false)
        {
            if (category)
            {
                categorymanager = new CategoryManager();
                Category result = categorymanager.GetCategoryByID(id);
                if (result.name == null) return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No such id");
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                categorymanager = new CategoryManager();

                if (limit < 1)
                {
                    limit = 10;
                }
                List<Question> result = categorymanager.GetQuestionsFromCategory(id, last, limit);
                if (result.Count == 0) {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No such id");      
                }
                return Request.CreateResponse<List<Question>>(HttpStatusCode.OK, result);
            }
        }

        [Authorize(Roles = "Administrator")]
        [HttpPut]
        public HttpResponseMessage RenameCategory(int id, Category category)
        {

            if (category != null)
            {
                categorymanager = new CategoryManager();
                HttpResponseMessage response = categorymanager.RenameCategory(id, category.name);
                return response;
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage PostCategory(Category category)
        {
            if (category != null)
            {
                categorymanager = new CategoryManager();
                var statusCode = categorymanager.CreateCategory(category);
                var response = Request.CreateResponse<object>(statusCode, new { id = category.id });
                response.Headers.Location = new Uri(Request.RequestUri.AbsoluteUri + "/" + category.id);
                return response;
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage DeleteCategory(int id)
        {
            categorymanager = new CategoryManager();
            HttpResponseMessage response = categorymanager.DeleteCategoryByID(id);
            return response;
        } 
    }
}
