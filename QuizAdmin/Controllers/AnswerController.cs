﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using QuizAdmin.Data;

namespace QuizAdmin.Controllers
{
    [Authorize]
    public class AnswerController : ApiController
    {
        AnswerManager answermanager;

        public IEnumerable<Answer> GetAnswersFromQuestion(int q)
        {
            answermanager = new AnswerManager();
            List<Answer> result = answermanager.GetAnswersFromQuestion(q);
            return result;
        }
        public HttpResponseMessage PostAnswer(string q, Answer answer)
        {
            if (answer != null)
            {
                answermanager = new AnswerManager();
                var statusCode = answermanager.CreateAnswer(q, answer);
                var response = Request.CreateResponse<object>(statusCode, new { id = answer.id });
                response.Headers.Location = new Uri(Request.RequestUri.AbsoluteUri + "/" + answer.id);
                return response;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.BadRequest);
                return response;
            }
            
        }

        [Authorize(Roles = "Administrator")]
        public HttpResponseMessage PutEditAnswer(int q, Answer answer)
        {
            if (answer != null)
            {
                answermanager = new AnswerManager();
                HttpResponseMessage response = answermanager.EditAnswer(q, answer);
                return response;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.BadRequest);
                return response;
            }
        }
        public HttpResponseMessage DeleteAnswer(string id)
        {
            answermanager = new AnswerManager();
            HttpResponseMessage response = answermanager.DeleteAnswerById(id);
            return response;
        }
    }
}
