﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using QuizAdmin.Data;

namespace QuizAdmin.Controllers
{
    [Authorize]
    public class HighscoreController : ApiController
    {
        HighscoreManager highscoremanager;

        public IEnumerable<Highscore> GetHighscore(int id, int limit = 10)
        {
            highscoremanager = new HighscoreManager();
            if (limit < 1)
            {
                limit = 10;
            }

            List<Highscore> result = highscoremanager.GetHighscoreByCategory(id, limit);
            return result;
        }
        public HttpResponseMessage PostHighscore(int id, Highscore highscore)
        {
            if (highscore != null)
            {
                highscoremanager = new HighscoreManager();
                HttpResponseMessage response = highscoremanager.PostHighscore(id, highscore);
                if (response.IsSuccessStatusCode) Authentication.CredentialsService.DestroySessionKeys(User.Identity.Name);
                return response;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.BadRequest);
                return response;
            }
        }
    }
}
