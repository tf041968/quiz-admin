﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using QuizAdmin.Data;

namespace QuizAdmin.Controllers
{
    //[Authorize]
    public class QuestionController : ApiController
    {
        
        QuestionManager questionmngr;

        public IEnumerable<Question> GetAllQuestions()
        {
            questionmngr = new QuestionManager();
            List<Question> results = questionmngr.GetQuestions();
            return results;
        }

        public Question GetQuestionById(int id)
        {
            questionmngr = new QuestionManager();
            Question question = questionmngr.GetQuestionById(id);
            return question;
        }

        //[Authorize(Roles="Administrator")]
        public HttpResponseMessage PostQuestion(Question question)
        {
            if (question != null)
            {
                questionmngr = new QuestionManager();
                var statusCode = questionmngr.PostQuestion(question);
                var response = Request.CreateResponse<object>(statusCode, new { id = question.id });
                response.Headers.Location = new Uri(Request.RequestUri.AbsoluteUri + "/" + question.id);
                return response;
            }
            else 
            {
                var response = Request.CreateResponse(HttpStatusCode.BadRequest);
                return response;
            }
            
        }


        [Authorize(Roles = "Administrator")]
        [HttpPut]
        public HttpResponseMessage ModifyQuestion(int id, Question question)
        {
            if (question != null)
            {
                questionmngr = new QuestionManager();
                HttpResponseMessage response = questionmngr.ModifyQuestion(id, question);
                return response;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.BadRequest);
                return response;
            }
        }

        public HttpResponseMessage DeleteQuestion(int id)
        {
            questionmngr = new QuestionManager();
            HttpResponseMessage response = questionmngr.DeleteQuestion(id);
            return response;
        }
    }
}
