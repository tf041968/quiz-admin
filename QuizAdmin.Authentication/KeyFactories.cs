﻿using System;

namespace QuizAdmin.Authentication {
	public abstract class KeyFactoryBase<TResult, T> {
		protected const int DefaultKeySize = 64;
		protected Picker<T> Picker { get; set; }

		public virtual TResult Create() {
			return Create(DefaultKeySize);
		}

		public abstract TResult Create(int size);
	}


	public abstract class ApiKeyFactoryBase : KeyFactoryBase<string, char> {
		protected ApiKeyFactoryBase() {
			Picker = new AlphaNumPicker();
		}
	}

	public class ApiKeyFactory : ApiKeyFactoryBase {
		public override string Create(int size) {
			return new String(Picker.Pick(size));
		}
	}

	public abstract class SecretFactoryBase : KeyFactoryBase<byte[], byte> {
		protected SecretFactoryBase() {
			Picker = new CryptoBytePicker();
		}
	}

	public class SecretFactory : SecretFactoryBase {
		public override byte[] Create(int size) {
			return Picker.Pick(size);
		}
	}
}
