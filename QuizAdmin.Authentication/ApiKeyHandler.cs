﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.Tracing;

namespace QuizAdmin.Authentication {
	public class ApiKeyHandler : DelegatingHandler {
		private static readonly Claim UserClaim  = new Claim(ClaimTypes.Role, "User");
		private static readonly Claim AdminClaim = new Claim(ClaimTypes.Role, "Administrator");

		private readonly ITraceWriter _trace;

		public ApiKeyHandler(HttpConfiguration config) {
			InnerHandler = new HttpControllerDispatcher(config);
			_trace = config.Services.GetTraceWriter();
		}

		protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken) {
			var auth = request.Headers.Authorization;
			var date = request.Headers.Date;

			_trace.Debug(request, "requests", "authentication request: {0}", request.ToString());
			if (date.HasValue && auth != null && auth.Scheme.Equals("Basic", StringComparison.InvariantCulture)) {
				var principal = Authenticate(request);
				if (principal != null) Thread.CurrentPrincipal = principal;
			}

			return base.SendAsync(request, cancellationToken)
				.ContinueWith(t => {
					var resp = t.Result;
					if (resp.StatusCode == HttpStatusCode.Unauthorized) {
						resp.Headers.Add("WWW-Authenticate", "Basic realm=\"quizadmin\"");
					}

					return resp;
				});
		}

		private IPrincipal Authenticate(HttpRequestMessage request) {
			var t = SplitIdentityString(request.Headers.Authorization.Parameter);
			if (t == null) {
				_trace.Debug(request, "authentication", "authorization parameter was not in format username:password");
				return null;
			}

			var sessionKey = CredentialsService.GetSessionKey(t.Item1);
			if (sessionKey == null || sessionKey.Length <= 0) {
				_trace.Debug(request, "authentication", "session key missing: {0}", t.Item1);
				return null;
			}

			var expected = t.Item2;
			var computed = GetSignature(request, sessionKey);
			if (!computed.Equals(expected, StringComparison.InvariantCulture)) {
				_trace.Debug(request, "authentication", "signature mismatch: expected {0} got {1}", expected, computed);
				return null;
			}

			var principal = CreatePrincipal(t.Item1);
			_trace.Debug(request, "authentication", "authenticated {0}", principal.Identity.Name);
			return principal;
		}

		/// <summary>
		/// Split identity string into a tuple consisting of <c>(key, signature)</c>
		/// </summary>
		/// <param name="s">identity string to split</param>
		/// <remarks>
		/// Identity string is assumed to be base64-encoded
		/// </remarks>
		internal static Tuple<string, string> SplitIdentityString(string s) {
			if (String.IsNullOrEmpty(s)) throw new ArgumentException("must not be null or empty", "s");

			const char separator = ':';
			var str = Encoding.UTF8.GetString(Convert.FromBase64String(s));
			var idx = Array.IndexOf(str.ToCharArray(), separator);
			if (idx < 0) return null;

			return Tuple.Create(str.Substring(0, idx), str.Substring(idx + 1));
		}

		/// <summary>
		/// Compute HMAC-SHA-1 signature for a request given a key
		/// </summary>
		internal static string GetSignature(HttpRequestMessage request, byte[] key) {
			var data = String.Join(" ", new[] {
				request.Method.Method,
				request.RequestUri.Scheme + "://" + request.Headers.Host + request.RequestUri.PathAndQuery,
				request.Headers.Date.Value.ToString("r")
			});

			using (var hmac = new HMACSHA1(key)) {
				var hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(data));
				return new String(hash.SelectMany(x => x.ToString("X2")
					.ToLower(CultureInfo.InvariantCulture))
					.ToArray());
			}
		}

		internal static IPrincipal CreatePrincipal(string username) {
			var nameClaim = new Claim(ClaimTypes.Name, username);
			var claims = CredentialsService.IsAdminKey(username)
				? new[] { UserClaim, AdminClaim, nameClaim }
				: new[] { UserClaim, nameClaim };

			return new ClaimsPrincipal(new ClaimsIdentity(claims, "api"));
		}
	}
}
