﻿using System;
using System.Security.Cryptography;

namespace QuizAdmin.Authentication {
	public abstract class Picker<T> {
		public abstract T Pick();
		public T[] Pick(int size) {
			if (size <= 0) throw new ArgumentOutOfRangeException("size", "must be greater than zero");
			return PickerImpl(size);
		}

		protected abstract T[] PickerImpl(int size);
	}

	public class AlphaNumPicker : Picker<char> {
		private static readonly char[] AllowedCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456890".ToCharArray();
		private readonly Random _rng = new Random();

		public override char Pick() {
			return AllowedCharacters[_rng.Next(AllowedCharacters.Length)];
		}

		protected override char[] PickerImpl(int size) {
			var chars = new char[size];
			for (var i = 0; i < size; i++) {
				chars[i] = Pick();
			}
			return chars;
		}
	}

	public class CryptoBytePicker : Picker<byte> {
		private static readonly RandomNumberGenerator CryptoRng = new RNGCryptoServiceProvider();

		public override byte Pick() {
			return PickerImpl(1)[0];
		}

		protected override byte[] PickerImpl(int size) {
			var bytes = new byte[size];
			CryptoRng.GetBytes(bytes);
			return bytes;
		}
	}
}
