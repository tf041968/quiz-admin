﻿using System;

using QuizAdmin.Data.Common;

namespace QuizAdmin.Authentication {
	public static class CredentialsService {
		private static readonly ApiKeyFactory ApiKeyFactory   = new ApiKeyFactory();
		private static readonly ApiCredentialsFactory Factory = new ApiCredentialsFactory(ApiKeyFactory, new SecretFactory());
		private static readonly ApiCredential Empty           = new ApiCredential();

		private static readonly IKeyStorageProvider KeyStorage =
			new KeyStorageProvider(ConnectionProviderManager.GetProvider<SqlConnectionProvider>());


		public static ApiCredential EmptyCredentials {
			get { return Empty; }
		}

		public static ApiCredential CreateCredentials() {
			var c = Factory.Create();
			return c;
		}

		public static string CreateSessionKey(string apiKey) {
			if (HasSessionKey(apiKey)) {
				var destroyed = DestroySessionKeys(apiKey);
				if (!destroyed) return String.Empty;
			}

			var key = ApiKeyFactory.Create(64);
			return KeyStorage.StoreSessionKey(apiKey, key)
				? key
				: String.Empty;
		}

		public static bool DestroySessionKeys(string apiKey) {
			var cred = KeyStorage.GetKey(apiKey);
			return KeyStorage.DestroySessionKeys(cred.Id);
		}

		public static bool HasSessionKey(string apiKey) {
			return GetSessionKey(apiKey) != null;
		}

		public static byte[] GetSessionKey(string key) {
			if (key == null) throw new ArgumentNullException("key", "must not be null");
			return KeyStorage.GetSessionKey(key);
		}

		public static bool IsAdminKey(string key) {
			var credentials = KeyStorage.GetKey(key);
			return credentials.Type == KeyType.Admin;
		}
	}
}