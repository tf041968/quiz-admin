﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using Dapper;
using NLog;

using QuizAdmin.Data.Common;

namespace QuizAdmin.Authentication {
	public interface IKeyStorageProvider {
		IConnectionProvider Provider { get; set; }
		byte[] GetSessionKey(string key);
		ApiCredential GetKey(string key);
		bool StoreSessionKey(string apiKey, string sessionKey);
		bool DestroySessionKeys(int id);
	}

	public class KeyStorageProvider : IKeyStorageProvider {
		private static readonly Logger        Logger           = LogManager.GetCurrentClassLogger();

		public IConnectionProvider Provider { get; set; }
		public KeyStorageProvider(IConnectionProvider provider) {
			if (provider == null) throw new ArgumentNullException("provider", "must not be null");

			Provider = provider;
		}

		public byte[] GetSessionKey(string key) {
			if (String.IsNullOrEmpty(key)) throw new ArgumentNullException("key", "must not be null or empty");

			using (var conn = Provider.GetConnection()) {
				try {
					conn.Open();
					var result = conn.Query<string>(
							@"SELECT s.SessionKey FROM SessionApiKeys s LEFT JOIN ApiKeys a ON s.ApiKey = a.Id WHERE a.Shared = @Key",
							new { Key = key })
							.ToList();
					if (result.Any()) {
						return Encoding.UTF8.GetBytes(result.First());
					}
				} catch (Exception ex) {
					if (ex is SqlException || ex is InvalidOperationException) {
						Logger.Error("Exception in QuizAdmin.Authentication.KeyStorageProvider.GetSessionKey (key={0})", key);
						Logger.Error(ex.ToString);
						return null;
					}
					throw;
				}
			}

			return null;
		}

		public ApiCredential GetKey(string key) {
			if (String.IsNullOrEmpty(key)) throw new ArgumentNullException("key", "must not be null or empty");

			using (var conn = Provider.GetConnection()) {
				try {
					conn.Open();
					return conn.Query<ApiCredential>(@"SELECT * FROM ApiKeys WHERE Shared = @Key", new { Key = key })
						.DefaultIfEmpty(CredentialsService.EmptyCredentials)
						.First();
				} catch (Exception ex) {
					if (ex is SqlException || ex is InvalidOperationException) {
						Logger.Error("Exception in QuizAdmin.Authentication.KeyStorageProvider.GetKey (key={0})", key);
						Logger.Error(ex.ToString);
						return CredentialsService.EmptyCredentials;
					}
					throw;
				}
			}
		}

		public bool StoreSessionKey(string apiKey, string sessionKey) {
			if (String.IsNullOrEmpty(apiKey)) throw new ArgumentNullException("apiKey", "must not be null or empty");

			var ak = GetKey(apiKey);
			if (ak == CredentialsService.EmptyCredentials) return false;

			using (var conn = Provider.GetConnection()) {
				try {
					conn.Open();
					var rows = conn.Execute(@"INSERT INTO SessionApiKeys VALUES (@Id, @Key)", new {
						Id = ak.Id,
						Key = sessionKey
					});
					return rows >= 1;
				} catch (Exception ex) {
					if (ex is SqlException || ex is InvalidOperationException) {
						Logger.Error("Exception in QuizAdmin.Authentication.KeyStorageProvider.StoreSessionKey (apikey={0}, sessionkey={1})", apiKey, sessionKey);
						Logger.Error(ex.ToString);
						return false;
					}
					throw;
				}
			}
		}

		public bool DestroySessionKeys(int id) {
			using (var conn = Provider.GetConnection()) {
				try {
					conn.Open();
					var rows = conn.Execute(@"DELETE FROM SessionApiKeys WHERE ApiKey = @Id", new { Id = id });
					return rows == 1;
				} catch (Exception ex) {
					if (ex is SqlException || ex is InvalidOperationException) {
						Logger.Error("Exception in QuizAdmin.Authentication.KeyStorageProvider.DestroySessionKeys (id={0})", id);
						Logger.Error(ex.ToString);
						return false;
					}
					throw;
				}
			}
		}
	}

	public enum KeyLifespan {
		ApiKey,
		SessionKey
	}
}