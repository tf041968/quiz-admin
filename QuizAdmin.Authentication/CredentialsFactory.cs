﻿namespace QuizAdmin.Authentication {
	public abstract class CredentialsFactoryBase {
		protected ApiKeyFactoryBase ApiKeyFactory { get; set; }
		protected SecretFactoryBase SecretFactory { get; set; }

		protected CredentialsFactoryBase(ApiKeyFactoryBase apiKeyFactory, SecretFactoryBase secretFactory) {
			ApiKeyFactory = apiKeyFactory;
			SecretFactory = secretFactory;
		}

		public abstract ApiCredential Create();
	}

	public class ApiCredential {
		public int     Id     { get; set; }
		public string  Shared { get; set; }
		public KeyType Type   { get; set; }
	}

	public enum KeyType : byte {
		Client,
		Admin
	}

	public class ApiCredentialsFactory : CredentialsFactoryBase {
		private const int KeySize = 64;
		public ApiCredentialsFactory(ApiKeyFactoryBase apiKeyFactory, SecretFactoryBase secretFactory)
			: base(apiKeyFactory, secretFactory) { }

		public override ApiCredential Create() {
			return new ApiCredential {
				Shared = ApiKeyFactory.Create(KeySize),
				//Secret = SecretFactory.Create(KeySize),
				//Salt   = SecretFactory.Create(KeySize)
			};
		}
	}
}