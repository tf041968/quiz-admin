﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace QuizAdmin.Authentication.Controllers {
	public class NewSessionController : ApiController {
		public HttpResponseMessage GetSessionKey(string id) {
			if (String.IsNullOrEmpty(id)) {
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "apikey missing");
			}

			var sessionKey = CredentialsService.CreateSessionKey(id);
			if (String.IsNullOrEmpty(sessionKey)) {
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "couldn't create session key");
			}

			return Request.CreateResponse(HttpStatusCode.OK, new {
				SessionKey = sessionKey
			});
		}
	}
}
