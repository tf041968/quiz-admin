﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.SqlServer;

namespace QuizAdmin.Data
{
    [DataContract]
    public class Category
    {
        [DataMember] public int id { get; set; }
        [DataMember] public string name { get; set; }
    }

    [DataContract]
    public class Question
    {
        [DataMember] public int id { get; set; }
        [DataMember] public string description { get; set; }
        [DataMember] public string media { get; set; } //BLOB? varchar (max) varbinary
        [DataMember] public string filename { get; set;  }
        [DataMember] public string mime { get; set; }
        [DataMember] public List<int> belongToCategory { get; set; }
    }

    [DataContract]
    public class Answer
    {
        [DataMember] public int     id { get; set; }
        [DataMember] public string  description { get; set; }
        [DataMember] public string  media { get; set; } //BLOB? varchar (max) varbinary
        [DataMember] public string  filename { get; set; }
        [DataMember] public string  mime { get; set; }
        [DataMember] public int     questionid { get; set; }
        [DataMember] public bool    correct { get; set; }
    }

    [DataContract]
    public class Highscore
    {
        [DataMember] public long     score { get; set; }
        [DataMember] public string  name { get; set; }
        [DataMember] public int     categoryId { get; set; }
    }

    public enum TableNames
    {
        Categories,
        Answers,
        Questions
    }
}
