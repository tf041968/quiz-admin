﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using QuizAdmin.Data.Common;

namespace QuizAdmin.Data
{
    public class DBConnection
    {
        SqlConnection connection;
        public DBConnection()
        { 
        
        
        }
        public SqlConnection GetSQLConnection()
        {         
            var provider = ConnectionProviderManager.GetProvider<SqlConnectionProvider>();
           
            using (var conn = provider.GetConnection())
            {
                
                try
                {
                    connection = new SqlConnection(conn.ConnectionString);
                    connection.Open();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            } 
            return connection;
        }
        public void CloseConnection()
        {
            connection.Close();
        }
    }
}
