﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Data;
using System.Data.SqlClient;

namespace QuizAdmin.Data
{
    public class QuestionManager
    {
        HttpStatusCode statusCode;
        SqlConnection sqlconnection;
        SqlCommand cmd;
        SqlDataReader reader;
        DBConnection dbConnection;

        public QuestionManager()
        {
            dbConnection = new DBConnection();
            sqlconnection = dbConnection.GetSQLConnection();
        }
        public List<Question> GetQuestions()
        {
            List<Question> result = new List<Question>();
            cmd = new SqlCommand();
            cmd.CommandText = "SELECT * FROM Questions";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlconnection;
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Question row = new Question();
                row.id = (int)reader["Id"];
                row.description = reader["Description"].ToString();
                row.filename = reader["Filename"].ToString();
                row.media = reader["Media"].ToString();
                row.mime = reader["Mime"].ToString();
                result.Add(row);
            }
            reader.Close();
            foreach (Question question in result)
            {
                question.belongToCategory = GetQuestionCategory(question);
            }

            sqlconnection.Close();
            return result;
        }

        public Question GetQuestionById(int id)
        {
            cmd = new SqlCommand();
            cmd.CommandText = "SELECT * FROM Questions WHERE Questions.Id=(@id)";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Connection = sqlconnection;
            reader = cmd.ExecuteReader();
           
            Question question = new Question();
            while (reader.Read())
            {
                question.id = (int)reader["Id"];
                question.description = reader["Description"].ToString();
                question.filename = reader["Filename"].ToString();
                question.media = reader["Media"].ToString();
                question.mime = reader["Mime"].ToString();
            }
            
            reader.Close();
            question.belongToCategory = GetQuestionCategory(question);
            sqlconnection.Close();
            return question;
        }

        /// <summary>
        /// Get the questions current categories
        /// </summary>
        /// <param name="question">The question to get categories from</param>
        /// <returns>List of all categories the question is represented in.</returns>
        public List<int> GetQuestionCategory(Question question)
        {
            cmd.Parameters.Clear();
            cmd.CommandText = "SELECT CategoryId FROM QuestionInCategory WHERE (@QuestionID)=QuestionId";
            cmd.Parameters.AddWithValue("@QuestionID", question.id);
            reader = cmd.ExecuteReader();

            List<int> questionInCategory = new List<int>();
            while (reader.Read())
            {
                if(reader["CategoryId"] != DBNull.Value)
                    questionInCategory.Add((int)reader["CategoryId"]);
            }
            reader.Close();
            return questionInCategory;
        }

        public HttpStatusCode PostQuestion(Question question)
        {
            statusCode = HttpStatusCode.Created;
            SqlCommand command = sqlconnection.CreateCommand();
            SqlTransaction transaction;
            transaction = sqlconnection.BeginTransaction("CreateQuestionTransaction");
            command.Connection = sqlconnection;
            command.Transaction = transaction;

            try
            {
                command.CommandText = "INSERT into Questions (Description, Filename, Media, Mime) VALUES (@Description, @Filename, @Media, @Mime)";
                command.Parameters.AddWithValue("@Description", question.description);
                command.Parameters.AddWithValue("@Filename", question.filename);
                command.Parameters.AddWithValue("@Media", question.media);
                command.Parameters.AddWithValue("@Mime", question.mime);
                command.ExecuteNonQuery();

                command.CommandText = "SELECT TOP 1 Id FROM Questions ORDER BY Id DESC";
                reader = command.ExecuteReader();
                int questionId = 0;
                while (reader.Read())
                {
                    questionId = (int)reader["Id"];
                }
                reader.Close();
                question.id = questionId;
                
                if (question.belongToCategory != null)
                {
                    foreach (int category in question.belongToCategory)
                    {
                        command = new SqlCommand();
                        command.Connection = sqlconnection;
                        command.Transaction = transaction;
                        command.CommandText = "Insert into QuestionInCategory (QuestionId, CategoryId) VALUES (@QuestionId, @InCategory)";
                        command.Parameters.AddWithValue("@QuestionId", questionId);
                        command.Parameters.AddWithValue("@InCategory", category);
                        command.ExecuteNonQuery();
                    }
                }
                transaction.Commit();
            }
            catch (Exception ex)
            {
                statusCode = HttpStatusCode.BadRequest;
                try
                {
                    transaction.Rollback();
                }
                catch (Exception ex2)
                {
                    //ERROR MESSAGE
                }
            }
            sqlconnection.Close();
            var response = new HttpResponseMessage(statusCode);

            return statusCode;
        }

        public HttpResponseMessage ModifyQuestion(int id, Question question)
        {
            statusCode = HttpStatusCode.OK;
            statusCode = HttpStatusCode.Created;
            SqlCommand cmd = sqlconnection.CreateCommand();
            SqlTransaction transaction;
            transaction = sqlconnection.BeginTransaction("CreateModifyTransaction");
            cmd.Connection = sqlconnection;
            cmd.Transaction = transaction;

            try
            {
                //Deletes the questions relation to categories
                cmd.CommandText = "DELETE FROM QuestionInCategory WHERE QuestionId=(@QuestionId)";
                cmd.Parameters.AddWithValue("@QuestionId", id);
                cmd.ExecuteNonQuery();

                //Updates the questions relation to categories
                cmd = new SqlCommand();
                cmd.Connection = sqlconnection;
                cmd.Transaction = transaction;
                if (question.belongToCategory.Count != 0)
                {
                    foreach (int category in question.belongToCategory)
                    {
                        cmd.CommandText = "INSERT INTO QuestionInCategory (QuestionId, CategoryId) VALUES (@QuestionId, @CategoryId)";
                        cmd.Parameters.AddWithValue("@QuestionId", id);
                        cmd.Parameters.AddWithValue("@CategoryId", category);
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                }

                //Updates the question 
                cmd = new SqlCommand();
                cmd.Connection = sqlconnection;
                cmd.Transaction = transaction;
                cmd.CommandText = "UPDATE Questions SET Description=(@Description), Filename=(@Filename), Media=(@Media), Mime=(@Mime) WHERE Id=(@id)";
                cmd.Parameters.AddWithValue("@Description", question.description);
                cmd.Parameters.AddWithValue("@Filename", question.filename);
                cmd.Parameters.AddWithValue("@Media", question.media);
                cmd.Parameters.AddWithValue("@Mime", question.mime);
                cmd.Parameters.AddWithValue("@id", id);
                int updatedRows = cmd.ExecuteNonQuery();
                if (updatedRows == 0)
                {
                    statusCode = HttpStatusCode.NotFound;
                }
                transaction.Commit();
            }

            catch (SqlException e)
            {
                statusCode = HttpStatusCode.BadRequest;
            }

            sqlconnection.Close();
            var response = new HttpResponseMessage(statusCode);
            return response;
        }

        public HttpResponseMessage DeleteQuestion(int id)
        {
            statusCode = HttpStatusCode.OK;
            string stmt = "DELETE FROM Questions WHERE Id = (@id)";
            SqlCommand cmd = new SqlCommand(stmt, sqlconnection);
            cmd.Parameters.AddWithValue("@id", id);
            try
            {
                int deletedRows = cmd.ExecuteNonQuery();
                if (deletedRows == 0)
                {
                    statusCode = HttpStatusCode.NotFound;
                }
            }
            catch (SqlException e)
            {
                switch (e.Number)
                {
                    case 2627:
                        statusCode = HttpStatusCode.Conflict;
                        break;
                }
            }
            sqlconnection.Close();
            return new HttpResponseMessage(statusCode);
        }
    }
}
