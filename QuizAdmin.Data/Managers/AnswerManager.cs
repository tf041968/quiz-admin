﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Data;
using System.Data.SqlClient;

namespace QuizAdmin.Data
{
    public class AnswerManager
    {
            HttpStatusCode statusCode;
            SqlConnection sqlconnection;
            SqlCommand cmd;
            SqlDataReader reader;
            DBConnection dbConnection;

            public AnswerManager() 
            { 
                dbConnection = new DBConnection();
                sqlconnection = dbConnection.GetSQLConnection();
            }

            public List<Answer> GetAnswersFromQuestion(int id)
            {
                List<Answer> result = new List<Answer>();

                cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Answers WHERE QuestionId=(@id)";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue(@"id", id);
                cmd.Connection = sqlconnection;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Answer row = new Answer();
                    row.id = (int)reader["Id"];
                    row.description = reader["Description"].ToString();
                    row.questionid = (int)reader["QuestionId"];
                    row.correct = (bool)reader["Correct"];
                    row.filename = reader["Filename"].ToString();
                    row.mime = reader["Mime"].ToString();
                    row.media = reader["Media"].ToString();
                    result.Add(row);
                }

                sqlconnection.Close();
                return result;
            }
        public HttpStatusCode CreateAnswer(string q, Answer answer)
        {
            statusCode = HttpStatusCode.Created;
            string stmt = "INSERT INTO Answers(Description, Correct, QuestionId, Filename, Media, Mime) VALUES (@Description, @Correct, @QuestionId, @Filename, @Media, @Mime)";
            SqlCommand cmd = new SqlCommand(stmt, sqlconnection);
            cmd.Parameters.AddWithValue("@Description", answer.description);
            cmd.Parameters.AddWithValue("@Correct", answer.correct);
            cmd.Parameters.AddWithValue("@QuestionId", q);
            cmd.Parameters.AddWithValue("@Filename", answer.filename);
            cmd.Parameters.AddWithValue("@Media", answer.media);
            cmd.Parameters.AddWithValue("@Mime", answer.mime);
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                statusCode = HttpStatusCode.BadRequest;
            }
            stmt = "SELECT TOP 1 Id FROM Answers ORDER BY Id DESC";
            cmd = new SqlCommand(stmt, sqlconnection);

            reader = cmd.ExecuteReader();
            int answerID = 0;
            while (reader.Read())
            {
                answerID = (int)reader["Id"];
            }
            reader.Close();
            answer.id = answerID;

            sqlconnection.Close();
            return statusCode;
        }
        public HttpResponseMessage EditAnswer(int q, Answer answer)
        {
            statusCode = HttpStatusCode.OK;
            string stm = "UPDATE Answers SET Description=(@Description), Correct=(@Correct), QuestionId=(@QuestionId), Filename=(@Filename), Mime=(@Mime), Media=(@Media)  WHERE id=(@id)";
            SqlCommand cmd = new SqlCommand(stm, sqlconnection);
            cmd.Parameters.AddWithValue("@Description", answer.description);
            cmd.Parameters.AddWithValue("@Correct", answer.correct);
            cmd.Parameters.AddWithValue("@QuestionId", answer.questionid);
            cmd.Parameters.AddWithValue("@id", q);
            cmd.Parameters.AddWithValue("@Filename", answer.filename);
            cmd.Parameters.AddWithValue("@Media", answer.media);
            cmd.Parameters.AddWithValue("@Mime", answer.mime);
            try
            {
                int updatedRows = cmd.ExecuteNonQuery();
                if (updatedRows == 0)
                {
                    statusCode = HttpStatusCode.NotFound;
                }

            }
            catch (SqlException e)
            {
                statusCode = HttpStatusCode.BadRequest;
            }
            sqlconnection.Close();
            return new HttpResponseMessage(statusCode);
        }
        public HttpResponseMessage DeleteAnswerById(string id)
        {
            statusCode = HttpStatusCode.OK;
            string stmt = "DELETE FROM Answers WHERE Id = (@id)";
            SqlCommand cmd = new SqlCommand(stmt, sqlconnection);
            cmd.Parameters.AddWithValue("@id", id);
            try
            {
                int deletedRows = cmd.ExecuteNonQuery();
                if (deletedRows == 0)
                {
                    statusCode = HttpStatusCode.NotFound;
                }
            }
            catch (SqlException e)
            {
                statusCode = HttpStatusCode.BadRequest;
            }
            sqlconnection.Close();
            return new HttpResponseMessage(statusCode);
        }
    }
}
