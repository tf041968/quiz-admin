﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Data;
using System.Data.SqlClient;

namespace QuizAdmin.Data
{
    public class CategoryManager
    {
        HttpStatusCode statusCode;
        SqlConnection sqlconnection;
        SqlCommand cmd;
        SqlDataReader reader;
        DBConnection dbConnection;
        
        public CategoryManager(){
            dbConnection = new DBConnection();
            sqlconnection = dbConnection.GetSQLConnection();
        }
        public List<Category> GetCategories()
        {
            List<Category> result = new List<Category>();
            cmd = new SqlCommand();
            cmd.CommandText = "SELECT * FROM Categories";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlconnection;
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Category row = new Category();
                row.id = (int)reader["Id"];
                row.name = reader["Name"].ToString();
                result.Add(row);
            }
            sqlconnection.Close();
            return result;
        }

        public Category GetCategoryByID(int id) 
        {
            cmd = new SqlCommand();
            cmd.CommandText = "SELECT * FROM Categories WHERE id=(@id)";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("id", id);
            cmd.Connection = sqlconnection;
            reader = cmd.ExecuteReader();

            Category cat = new Category();
            while (reader.Read())
            {
                cat.id = (int)reader["Id"];
                cat.name = reader["Name"].ToString();
            }
            sqlconnection.Close();
            return cat;
        }

        public List<Question> GetQuestionsFromCategory(int categoryID, string last, int limit)
        {

            List<Question> questions = new List<Question>();
            cmd = new SqlCommand();
            if (last == "true")
            {
                cmd.CommandText = "SELECT TOP (@Limit) * FROM Questions INNER JOIN QuestionInCategory ON QuestionInCategory.CategoryId=(@CategoryID) WHERE Questions.Id=QuestionInCategory.QuestionId ORDER BY Questions.Id DESC";
            }
            else
            {
                cmd.CommandText = "SELECT TOP (@Limit) * FROM Questions INNER JOIN QuestionInCategory ON QuestionInCategory.CategoryId=(@CategoryID) WHERE Questions.Id=QuestionInCategory.QuestionId ORDER BY newid()";
                //cmd.CommandText = "SELECT TOP (@Limit) * FROM Questions INNER JOIN Categories ON Categories.name=(@CategoryName) INNER JOIN QuestionInCategory ON Categories.id = QuestionInCategory.CategoryId WHERE Questions.Id=QuestionInCategory.QuestionId ORDER BY newid()";
            }
            cmd.Parameters.AddWithValue(@"CategoryID", categoryID);
            cmd.Parameters.AddWithValue(@"Last", last);
            cmd.Parameters.AddWithValue(@"Limit", limit);

            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlconnection;
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
               Question row = new Question();
               row.id = (int)reader["Id"];
               row.description = reader["Description"].ToString();
               row.filename = reader["Filename"].ToString();
               row.media = reader["Media"].ToString();
               row.mime = reader["Mime"].ToString();
               questions.Add(row);
            }
            reader.Close();
            foreach (Question question in questions)
            {
                cmd.CommandText = "SELECT CategoryId FROM QuestionInCategory WHERE (@QuestionID)=QuestionId";
                cmd.Parameters.AddWithValue("@QuestionID", question.id);
                reader = cmd.ExecuteReader();
                cmd.Parameters.Clear();
                List<int> questionInCategory = new List<int>();
                while (reader.Read())
                {
                    questionInCategory.Add((int)reader["CategoryId"]);
                }
                reader.Close();
                question.belongToCategory = questionInCategory;
            }

            sqlconnection.Close();
            return questions;
        }

        public HttpStatusCode CreateCategory(Category category)
        {
            statusCode = HttpStatusCode.Created;
            string stmt = "INSERT INTO Categories(name) VALUES(@Name)";
            SqlCommand cmd = new SqlCommand(stmt, sqlconnection);
            cmd.Parameters.AddWithValue("@Name", category.name);
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                switch (e.Number)
                {
                    case 2627:
                        statusCode = HttpStatusCode.Conflict;
                        break;
                }
            }

            stmt = "SELECT TOP 1 Id FROM Categories ORDER BY Id DESC";
            cmd = new SqlCommand(stmt, sqlconnection);

            reader = cmd.ExecuteReader();
            int categoryID = 0;
            while (reader.Read())
            {
                categoryID = (int)reader["Id"];
            }
            reader.Close();
            category.id = categoryID;



            sqlconnection.Close();
            return statusCode;
        }

        public HttpResponseMessage RenameCategory(int id, string newName)
        {
            statusCode = HttpStatusCode.OK;
            string stm = "UPDATE Categories SET name=(@NewName) WHERE Id=(@id)";
            SqlCommand cmd = new SqlCommand(stm, sqlconnection);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@NewName", newName);
            try
            {
                int updatedRows = cmd.ExecuteNonQuery();
                if (updatedRows == 0)
                {
                    statusCode = HttpStatusCode.NotFound;
                }

            }
            catch (SqlException e)
            {
                switch (e.Number)
                {
                    case 2627:
                        statusCode = HttpStatusCode.Conflict;
                        break;
                }
            }
            sqlconnection.Close();
            return new HttpResponseMessage(statusCode);
        }

        public HttpResponseMessage DeleteCategoryByID(int categoryID)
        {
            statusCode = HttpStatusCode.OK;
            string stm = "DELETE FROM Categories WHERE Id=(@categoryID)";
            SqlCommand cmd = new SqlCommand(stm, sqlconnection);
            cmd.Parameters.AddWithValue("@categoryID", categoryID);
            try
            {
                int deletedRows = cmd.ExecuteNonQuery();
                if (deletedRows == 0)
                {
                    statusCode = HttpStatusCode.NotFound;
                }
            }
            catch (SqlException e)
            {
                switch (e.Number)
                {
                    case 2627:
                        statusCode = HttpStatusCode.Conflict;
                        break;
                }
            }
            sqlconnection.Close();
            return new HttpResponseMessage(statusCode);
        }
    }
}