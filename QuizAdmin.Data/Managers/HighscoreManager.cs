﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Data;
using System.Data.SqlClient;

namespace QuizAdmin.Data
{
    public class HighscoreManager
    {
        HttpStatusCode statusCode;
        SqlConnection sqlconnection;
        SqlCommand cmd;
        SqlDataReader reader;
        DBConnection dbConnection;

        public HighscoreManager() 
        { 
            dbConnection = new DBConnection();
            sqlconnection = dbConnection.GetSQLConnection();
        }

        public List<Highscore> GetHighscoreByCategory(int id, int limit)
        {
            List<Highscore> result = new List<Highscore>();

            cmd = new SqlCommand();
            cmd.CommandText = "SELECT TOP (@Limit) * FROM Highscore WHERE CategoryId=(@Id) ORDER BY Score DESC";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue(@"Id", id);
            cmd.Parameters.AddWithValue(@"Limit", limit);
            cmd.Connection = sqlconnection;
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Highscore row = new Highscore();
                row.score = (long)reader["Score"];
                row.name = reader["Name"].ToString();
                row.categoryId = (int)reader["CategoryId"];
                result.Add(row);
            }

            sqlconnection.Close();
            return result;
        }
        public HttpResponseMessage PostHighscore(int category, Highscore highscore)
        {
            statusCode = HttpStatusCode.Created;
            
            string stmt = "INSERT INTO highscore (Score, Name, CategoryId) VALUES (@Score, @Name, @Category)";
            SqlCommand cmd = new SqlCommand(stmt, sqlconnection);
            cmd.Parameters.AddWithValue("@Score", highscore.score);
            cmd.Parameters.AddWithValue("@Name", highscore.name);
            cmd.Parameters.AddWithValue("@Category", category);
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                statusCode = HttpStatusCode.BadRequest;
            }

            sqlconnection.Close();
            return new HttpResponseMessage(statusCode);
        }
    }
}
