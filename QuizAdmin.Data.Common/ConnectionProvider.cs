﻿using System.Collections.Immutable;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace QuizAdmin.Data.Common {
	public interface IConnectionProvider {
		IDbConnection GetConnection();
	}

	public class SqlConnectionProvider : IConnectionProvider {
		private const string DefaultConnectionString = @"Data Source=(LocalDb)\Projects;Initial Catalog=quizadmin;Integrated Security=true;AttachDbFileName=|DataDirectory|quizadmin.mdf";
		private readonly string _connectionString;

		public SqlConnectionProvider(string connectionString = null) {
			_connectionString = connectionString ?? DefaultConnectionString;
		}

		public IDbConnection GetConnection() {
			return new SqlConnection(_connectionString);
		}
	}

	public static class ConnectionProviderManager {
		private static ImmutableHashSet<IConnectionProvider> _providers = ImmutableHashSet.Create<IConnectionProvider>();

		public static IConnectionProvider GetProvider<T>() where T : IConnectionProvider {
			return _providers.OfType<T>().FirstOrDefault();
		}

		public static void AddProvider(IConnectionProvider provider) {
			_providers = _providers.Add(provider);
		}
	}
}