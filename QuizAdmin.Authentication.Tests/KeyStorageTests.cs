﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

using QuizAdmin.Data.Common;

namespace QuizAdmin.Authentication.Tests {
	[TestFixture]
	public class KeyStorageTests {
		[TestFixtureSetUp]
		public void FixtureSetup() {
			ConnectionProviderManager.AddProvider(new SqlConnectionProvider());
		}

		[Test]
		public void GetConnection() {
			var provider = ConnectionProviderManager.GetProvider<SqlConnectionProvider>();
			using (var conn = provider.GetConnection()) {
				Console.WriteLine(conn.ConnectionString);
			}
		}
	}
}
