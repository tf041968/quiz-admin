﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

namespace QuizAdmin.Authentication.Tests {
	[TestFixture]
	class ApiKeyHandlerTests {
		[TestCase("aGVsbG86d29ybGQ=",     Result = new[] { "hello", "world" })]
		[TestCase("cGjhu586b21ub21ub20=", Result = new[] { "phở", "omnomnom" })]
		[TestCase("cGjhu586cGjhu58=",     Result = new[] { "phở", "phở" })]
		[TestCase(null, ExpectedException = typeof(ArgumentException))]
		[TestCase("",   ExpectedException = typeof(ArgumentException))]
		public string[] SplitIdentityString(string s) {
			var t = ApiKeyHandler.SplitIdentityString(s);
			return new[] { t.Item1, t.Item2 };
		}
	}
}
